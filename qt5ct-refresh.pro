QT += gui

CONFIG += c++17 console
CONFIG -= app_bundle

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += main.cpp

# Default rules for deployment.
unix:!android: target.path = /usr/bin
!isEmpty(target.path): INSTALLS += target
