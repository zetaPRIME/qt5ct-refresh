#include <QStandardPaths>
#include <QSettings>
#include <QFont>

int main(int argc [[maybe_unused]], char *argv [[maybe_unused]] []) {
    QFont f; // force final product to actually include QFont serialization
    QSettings cfg(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation) + QLatin1String("/qt5ct/qt5ct.conf"), QSettings::IniFormat);
    cfg.setValue("Appearance/style", cfg.value("Appearance/style")); // just force a sync
}
